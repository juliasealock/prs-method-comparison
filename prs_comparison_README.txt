PRS Method Comparison Script

This script is a wrapper to submit slurm jobs on a cluster that will generate polygenic scores for a target set using PRSice, PRS-CS-auto, and LDPred, allowing you to choose p-value thresholds, fraction causal, LD reference panels. 

Please let me know if there are any bugs, or if you have comments/suggestions to make the pipeline better - julia.m.sealock@vanderbilt.edu 

For details on the individual methods:

PRSice: https://choishingwan.github.io/PRSice/ 
PRS-CS: https://github.com/getian107/PRScs
LDPred: https://github.com/bvilhjal/ldpred

To get started:

git clone https://juliasealock@bitbucket.org/juliasealock/prs-method-comparison.git
cd prs-method-comparison
git clone https://github.com/bvilhjal/ldpred.git
git clone https://github.com/getian107/PRScs.git
git clone https://juliasealock@bitbucket.org/juliasealock/prsice.git
git clone https://juliasealock@bitbucket.org/juliasealock/plink.git


Options: 
1. pheno_name <- "name_of_phenotype"
2. out_dir <- '/path/to/write/files/'
3. target_dat <- '/prefix/of/plink/files/to/be/scored'
4. script_dir <- "/path/to/where/you/saved/the/scripts/"
5. email <- "your.email@vanderbilt.edu"
6. accre_account <- "accre_group"
7. PRSice <- "TRUE/FALSE"
8. prsice_base_dat <- '/path/to/sumstats/for/prsice'
9. pval_levels <- c("pval,thresholds,for,prsice")
10. PRSCS <- "TRUE/FALSE"
11. prs_cs_base_dat <- "//path/to/sumstats/for/prscs'
12. prs_cs_ld_ref_dir <- "/path/to/prscs/ld/reference/panel"
13. n_gwas <- 'n_in_gwas'
14. LDPred <- "TRUE/FALSE"
15. statistic <- "BETA/OR"
16. ldpred_fraction <- c("fractions causal for ldpred")
17. ldpred_ld_ref <- "/path/to/ldpred/reference/panel"
18. source(paste0(script_dir,"prs_comparison.R")

Lots of options so here are some descriptions:

1. name of your phenotype of interest
2. this is where folders will be created for each prs method to write results
3. path to plink bed/bim/fam files you want to have PRS generated for
4. directory where you pulled the scripts
5. your email
6. name of group on accre
7. do you want prsice to run? If yes --> "TRUE" ; if no, "FALSE" and you don't need to specifiy 7-8
8. path to your prsice sumstats (this is also needed if you want to run ldpred!)
9. pvalue thresholds you want generated from PRSice (must have commas in between values)
10. do you want to run prscs? If yes --> "TRUE" ; if no, "FALSE" and you don't need 10-11
11. path to prs-cs base dat (I know it's annoying to have both but prscs has a specific format: SNP A1 A2 BETA/OR/Z P. no more, no less)
12. path to prs-cs ld reference panels. download the files here: https://github.com/getian107/PRScs . The path must contain a subdirectory "ldblk_1kg" that contains the ld .hdf5 files
13. number of people in the gwas (this is also needed for ldpred!)
14. do you want to run ldpred? If yes --> "TRUE" ; if no, "FALSE" and you don't need to specifiy 14-16
15. do your sumstats have a beta or OR?
16. fraction of causal snps you want to try for ldpred (do not put commas in between values!)
17. path to plink files for ldpred reference set of matching ancestry as your target
18. call the script! 

To run PRSice what do I need?
1-9,13,17

To run PRS-CS what do I need?
1-7,9-13,17

To run LDPred what do I need?
1-7,9,12-17


PRSice Output
In the prsice directory of your out_dir you'll find the files:

*.slurm - slurm job created to run prsice
*output.txt - output from slurm job 
*.log - prsice's log 
*.all.score - file containing polygenic scores

PRS-CS Output
In the prs_cs directory of your out_dir you'll find the files:
*.slurm - input for prscs slurm job
*output.txt - output of slurm job 
pst_eff_a1_b0.5_phiauto_chr*.txt - results from MCMC for each chromosome
*_prs_cs_gwas.txt - concatenated file of chromosomes (i.e. the PRS-CS adjusted sumstats)
*.log - plink --score log
*.profile - file containing polygenic scores 

LDPred Output
*.slurm - input for ldpred slurm job
*output.txt - output for slurm job
*.hdf5 - ldpred coordinated sumstats 
*.pkl - ld information (auto ld radius of 1666)
*_ldpred_prs_results.txt - PRS results from all fractions specified



Example Command:

## IN R ON ACCRE:

pheno_name <- "auditp_mvp_pgc_ukb"
out_dir <- '/data/davis_lab/sealockj/projects/scripts/prs_comparison/test/'
target_dat <- '/data/davis_lab/sealockj/directly_genotyped_mega/mega_directly_genotyped_qced_0309'
script_dir <- "/data/davis_lab/sealockj/projects/scripts/prs_comparison/base_scripts/prs-method-comparison/"
email <- "julia.m.sealock@vanderbilt.edu"
accre_account <- "davis_lab"
PRSice <- "TRUE"
pval_levels <- c("5e-8,0.0005,0.05,1")
prsice_base_dat <- '/data/davis_lab/dennisj/biomarkers/data/GWAS_summary_stats/lipids/jointGwasMc_LDL_minRVals_clean.txt'
PRSCS <- "TRUE"
prs_cs_base_dat <- "/data/davis_lab/sealockj/projects/PRSice/AUDIT/audit_mvp/pgc_ukbb_mvp_meta/MVP_PGC_UKB_metal_update_0605.out_rsid_mapped_prscs.txt"
n_gwas <- '448876'
prs_cs_ld_ref_dir <- "/data/davis_lab/sealockj/projects/scripts/prs_comparison/base_scripts/prscs/ld_files/eur_ld"
LDPred <- "TRUE"
statistic <- "BETA"
ldpred_fraction <- c("1 0.1 0.01 0.001 0.0001")
ldpred_ld_ref <- "/scratch/davis_lab/sealocjm/eur_1kgp_phase3_ref_set"
source(paste0(script_dir,"prs_comparison.R"))

